package pokemonuninpahu;

import java.util.Scanner;

/**
 *
 * @author dleon
 */
public class Pokemon {

    private int vida, ataque, defensa;
    private String PokeNombre;

    public Pokemon(String PokeNombre, int Ataque, int Defensa) {
        this.PokeNombre = PokeNombre;
        this.vida=30;
        this.ataque=Ataque;
        this.defensa=Defensa;
        
        
    }

    public Pokemon() {
    }   

    public int getVida() {
        return vida;
    }

    public void setVida(int vida) {
        this.vida = vida;
    }
   
    
    
    public int getAtaque() {
        return ataque;
    }

    public void setAtaque(int ataque) {
        this.ataque = ataque;
    }

    public int getDefensa() {
        return defensa;
    }

    public void setDefensa(int defensa) {
        this.defensa = defensa;
    }

    public String getPokeNombre() {
        return PokeNombre;
    }

    public void setPokeNombre(String PokeNombre) {
        this.PokeNombre = PokeNombre;
    }

    public void AtaqueRecibido(int PoderAtaque){
    int CantidadAtaque= PoderAtaque;
    this.vida-=CantidadAtaque;
    
    }
      
    public String toString(){
    
    return "\n|| Nombre: "+this.PokeNombre+"\n|| vida: "+this.vida+"\n|| Ataque: "+this.ataque+"\n|| Defensa: "+this.defensa;
    
    }
}
