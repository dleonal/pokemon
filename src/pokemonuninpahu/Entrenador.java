
package pokemonuninpahu;
import java.util.Scanner;
import java.util.ArrayList;
/**
 *
 * @author dleon
 */
public class Entrenador {
    
    private int codigo;
    private String nombre;
    private int victorias;
    private Pokemon[] pokemones;
    private Pokemon pokemon;
   
    
    public Entrenador(int Codigo, String nombre, Pokemon[] listaPokemones){
         this.codigo = Codigo;
        this.nombre = nombre;        
        this.victorias = 0;
        this.pokemones = listaPokemones;
        
        
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Pokemon getPokemon() {
        return pokemon;
    }

    public void setPokemon(Pokemon pokemon) {
        this.pokemon = pokemon;
    }

    public Pokemon[] getPokemones() {
        return pokemones;
    }

    public void setPokemones(Pokemon[] pokemones) {
        this.pokemones = pokemones;
    }
    
    public void Opciones(){
        int opcion;
        do {
            Scanner leer = new Scanner(System.in);
            System.out.println("MENU OPCIONES ENTRENADOR");
            System.out.println("1) Selecionar un pokemon");
            System.out.println("2) salir");
            opcion = leer.nextInt();
            switch (opcion) {
                case 1:
                    for (int i = 0; i < pokemones.length; i++) {
                        Pokemon pok = pokemones[i];
                        System.out.println("numero: " + i + "  " + pok);
                    }
                    System.out.println("Cual pokemos quieres: ");
                    try{
                        int op = leer.nextInt();
                        System.out.println("tu Seleccion: " + pokemones[op]);
                        Pokemon pokemon = pokemones[op];
                        setPokemon(pokemon);
                    }catch(Exception e)
                    {
                        System.out.println("NO TIENES ESTE POKEMON EN TU LISTA");
                    }                    
                    break;
            }
        } while (opcion != 2);
    }

    public String toString() {

        return "Nombre" + getNombre();

    }
}
