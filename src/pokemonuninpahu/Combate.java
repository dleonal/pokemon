
package pokemonuninpahu;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Random;

/**
 *
 * @author dleon
 */
public class Combate {
    
    private ArrayList<Entrenador>entrenadores;
    private ArrayList<Pokemon>Pokemon;
    private Scanner leer;
    
    public Combate() {
         
         leer = new Scanner(System.in);
         Pokemon = new ArrayList<Pokemon>();
         entrenadores= new ArrayList<Entrenador>();
         Pokemon = Pokemones();
    }
    
    public ArrayList<Entrenador>RegistroEntrenador(){
        int contador = 0;
        boolean entrenadoresRequeridos = true;
        int codigo = 0;
        while (entrenadoresRequeridos) {
            System.out.println("       ¡¡BIENVENIDO AL TORNEO POKEMON!!\n "
                              +"Este torneo es para 6 Entrenadores\n"
                              +"Sabiendo esto pulse 1 para agregar un entrenador\n"
                              +"Si ya tiene los entrenadores suficientes, pulse 0\n"
                              +"Si quiere salir, pulse 2");
            int op = leer.nextInt();
            
            if (op == 0 && contador < 6) {
                int restante = 6 - contador;
                System.out.println("El torneo tiene una capacidad para 6 entrenadores, Se debe registrar " + restante + " Entrenador/es mas!!");
            } else if (op == 0 && contador >=6) {
                entrenadoresRequeridos = false;
                break;
            }else if (op==2){
                System.exit(0);
            }

            Pokemon[] pokemon = new Pokemon[3];
            String nombreEntrenador;
            

            System.out.println("Ingrese el nombre de tu Entrenador:");
            nombreEntrenador = leer.next();
            for (int i = 1; i < Pokemon.size(); i++) {
                Pokemon get = Pokemon.get(i);

                System.out.println(" Pokemon "+i+": " + get.toString());
            }
            System.out.println("            ¡¡La lista Pokemon!! \n"
                              +"Seleciona tres de ellos, por cada entrenador: ");
            for (int i = 0; i < pokemon.length; i++) {
                try {
                    int numero = leer.nextInt();
                    pokemon[i] = Pokemon.get(numero);
                } catch (Exception e) {
                    System.out.println("                 ¡¡Error!!"
                                      +"Este Pokemon no se encuentra en nuestra lista");
                }

            }
            entrenadores.add(new Entrenador(codigo, nombreEntrenador, pokemon));
            contador++;
            codigo++;
        }
        return entrenadores;
    }
   
    public int Combate(Entrenador entrenador1, Entrenador entrenador2){
        
        Random ganador = new Random(System.nanoTime());
        System.out.println("                         Batalla entre: \n"
                          +entrenador1.getNombre()+" | "+"contra"+" | "+entrenador2.getNombre());
        boolean turno=true;
        System.out.println("Turo para: "+" "+entrenador1.getNombre());
        entrenador1.Opciones();
        System.out.println("Turo para: "+" "+entrenador2.getNombre());
        entrenador2.Opciones();
        
        Pokemon pokemon1 = entrenador1.getPokemon();
        Pokemon pokemon2 = entrenador2.getPokemon();
        
        while(turno == true){
            System.out.println("el pokemon: " + " "+pokemon1.getPokeNombre() +" "+ "¡Ataca!");
            pokemon2.AtaqueRecibido(ganador.nextInt(pokemon1.getAtaque()));
            System.out.println( pokemon2.getPokeNombre() +" " +" tiene una vida de: " +" "+ pokemon2.getVida());
            if (pokemon2.getVida() == 0) {
                System.out.println("       ¡¡El Entrenador " +" "+entrenador1.getNombre() +" "+ "ganó la batalla!!");
                pokemon1.setVida((pokemon1.getVida() * 50)/100);
                turno = false;
                return entrenador2.getCodigo();
            } else {
                System.out.println("el pokemon: " +" "+pokemon2.getPokeNombre() +" "+" ¡Ataca!");
                pokemon1.AtaqueRecibido(ganador.nextInt(pokemon2.getAtaque()));
                System.out.println( pokemon1.getPokeNombre() + "tiene una vida de: " +" "+pokemon1.getVida());
                if (pokemon1.getVida() <= 0) {
                    System.out.println("   ¡¡El Entrenador " +" "+ entrenador2.getNombre() +" "+ "ganó la batalla!!");
                    pokemon2.setVida((pokemon2.getVida() * 50)/100);
                    turno = false;
                    return entrenador1.getCodigo();
                }
            }
        }
                    
        
   return 0;   
    }
   
   public  ArrayList<Pokemon> Pokemones() {

        ArrayList<Pokemon> pokemones = new ArrayList<Pokemon>();

        pokemones.add(new Pokemon("Picachu ", 6, 2));  pokemones.add(new Pokemon("Druddigon ", 7, 6));  pokemones.add(new Pokemon("Onix", 4, 5));
        pokemones.add(new Pokemon("Dodrio ", 8, 7)); pokemones.add(new Pokemon("Gengar", 1, 4));  pokemones.add(new Pokemon("Sawk", 4, 2));
        pokemones.add(new Pokemon("Piplup", 3, 2));   pokemones.add(new Pokemon("Ekans", 7, 6));  pokemones.add(new Pokemon("Primeape", 4, 2));
        pokemones.add(new Pokemon("Tepig", 3, 7));     pokemones.add(new Pokemon("Nidorino", 4, 8)); pokemones.add(new Pokemon("Mew", 2, 5));
        pokemones.add(new Pokemon("Mankey", 9, 4));      pokemones.add(new Pokemon("Riolu", 3, 8));   pokemones.add(new Pokemon("Golem", 4, 6));
        pokemones.add(new Pokemon("Wobbuffet", 9, 8));   pokemones.add(new Pokemon("Entei", 6, 1));    pokemones.add(new Pokemon("MewTwo", 3, 2));
        pokemones.add(new Pokemon("Marowak", 1, 9));
        

        return pokemones;
    }
   
   
   
   
   
   
   
   
   
}





    
    
    
    
    
    

